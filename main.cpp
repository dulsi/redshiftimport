#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include <zlib.h>

#include "readpng.h"   /* typedefs, common macros, readpng prototypes */

#define PROGNAME  "redshiftimport"
#define CHUNK 1024

static FILE *infile;

static ulg image_width, image_height, image_rowbytes;
static int image_channels;
static uch *image_data;

class ImportData
{
public:
	ImportData() : bit(0), val(0) {};

	void addBit(uch add);
	void write(const std::string &filename);

private:
	int bit;
	uch val;
	std::vector<uch> data;
};

void ImportData::addBit(uch add)
{
	val = val | (add << bit);
	bit++;
	if (bit == 8)
	{
		bit = 0;
		data.push_back(val);
		val = 0;
	}
}

void ImportData::write(const std::string &filename)
{
	int ret;
	unsigned have;
	z_stream strm;
	unsigned char in[CHUNK];
	unsigned char out[CHUNK];
	/* allocate inflate state */
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.avail_in = 0;
	strm.next_in = Z_NULL;
	ret = inflateInit(&strm);
	if (ret != Z_OK)
		return ;
	strm.avail_in = *(reinterpret_cast<int *>(&data[0]));
	strm.next_in = &data[8];

	FILE *f = fopen(filename.c_str(), "wb");
	if (f)
	{
		do {
			strm.avail_out = CHUNK;
			strm.next_out = out;
			ret = inflate(&strm, Z_NO_FLUSH);
//			assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
			switch (ret) {
			case Z_NEED_DICT:
				ret = Z_DATA_ERROR;     /* and fall through */
			case Z_DATA_ERROR:
			case Z_MEM_ERROR:
				(void)inflateEnd(&strm);
				return ; //ret;
			}
			have = CHUNK - strm.avail_out;
			fwrite(out, 1, have, f);
		} while (strm.avail_out == 0);
		fclose(f);
	}
}

int main(int argc, char **argv)
{
	int rc;
	int error = 0;
	if (argc < 2)
	{
		std::fprintf(stderr, "Insufficient arguments.\n");
		return 1;
	}
	char *filename = argv[1];
	if (!(infile = std::fopen(filename, "rb")))
	{
		std::fprintf(stderr, PROGNAME ":  can't open PNG file [%s]\n", filename);
		++error;
	}
	else
	{
		if ((rc = readpng_init(infile, &image_width, &image_height)) != 0) {
			switch (rc) {
			case 1:
				std::fprintf(stderr, PROGNAME
					":  [%s] is not a PNG file: incorrect signature\n",
				filename);
				break;
			case 2:
				std::fprintf(stderr, PROGNAME
					":  [%s] has bad IHDR (libpng longjmp)\n", filename);
				break;
			case 4:
				std::fprintf(stderr, PROGNAME ":  insufficient memory\n");
				break;
			default:
				std::fprintf(stderr, PROGNAME
					":  unknown readpng_init() error\n");
				break;
			}
			++error;
		}
		if (error)
			std::fclose(infile);
	}

	if (error) {
		std::fprintf(stderr, PROGNAME ":  aborting.\n");
		exit(2);
	}
	image_data = readpng_get_image(1, &image_channels, &image_rowbytes);
	readpng_cleanup(FALSE);

	ImportData import;
	uch val = 0;
	int bit = 0;
	for (int i = 0; i < image_height; i++)
	{
		for (int j = 0; j < image_width; j++)
		{
			import.addBit(image_data[image_rowbytes * i + j * 4] & 0x01);
			import.addBit(image_data[image_rowbytes * i + j * 4 + 1] & 0x01);
			import.addBit(image_data[image_rowbytes * i + j * 4 + 2] & 0x01);
		}
	}
	import.write("output.solution");
	return 0;
}
